"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses";
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        dataType: 'json',
        success: function (paramResponse) {
            console.log(paramResponse);
            displayCoursesToPopularList(paramResponse);
            displayCoursesToTrendingList(paramResponse);
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText);
        }
    });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm hiển thị khóa học ở vùng Popular:
function displayCoursesToPopularList(paramListCourses) {
    var vIndexCard = 1;
    for (var i = 0; i < paramListCourses.length && vIndexCard < 5; i++) {
        if (paramListCourses[i].isPopular) {
            displayPopularCoursesToCard(paramListCourses[i]);
            vIndexCard++;
        }
    }
}
//Hàm hiển thị khóa học ở vùng Trending:
function displayCoursesToTrendingList(paramListCourses) {
    var vIndexCard = 1;
    for (var i = 0; i < paramListCourses.length && vIndexCard < 5; i++) {
        if (paramListCourses[i].isTrending) {
            displayTrendingCoursesToCard(paramListCourses[i]);
            vIndexCard++;
        }
    }
}
//Hàm hiển thị Popular course lên thẻ: 
function displayPopularCoursesToCard(paramCourse) {
    var vPopularList = $("#popular-cards");
    //Tạo card mới:
    var vCardCourse = $("<div></div>").addClass("card").appendTo(vPopularList);
    //Tạo ảnh đại diện khóa học 
    $("<img></img>").addClass("card-img-top")
        .attr("src", paramCourse.coverImage)
        .appendTo(vCardCourse);
    //Tạo card body:
    var vCardBody = $("<div></div>").addClass("card-body").appendTo(vCardCourse);
    $("<h6></h6>").addClass("card-title text-primary")
        .html(paramCourse.courseName)
        .appendTo(vCardBody);
    $("<p></p>").addClass("card-text")
        .html('<i class="far fa-clock"> ' + paramCourse.duration + " " + paramCourse.level)
        .appendTo(vCardBody);
    $("<p></p>").addClass("card-text font-weight-bold")
        .html("$" + paramCourse.discountPrice + ' <del class="text-muted font-weight-normal">' + "$" + paramCourse.price + '</del> ')
        .appendTo(vCardBody);
    //Tạo footer cho card:
    var vCardFooter = $("<div></div>").addClass("card-footer").appendTo(vCardCourse);
    var vFooter = $("<div></div>").addClass("row").appendTo(vCardFooter);
    var vNewColumnTeacher = $("<div></div>").addClass("col-sm-10").appendTo(vFooter);
    $("<img></img>").attr("src", paramCourse.teacherPhoto)
        .addClass("rounded-circle size-avatar mr-2")
        .appendTo(vNewColumnTeacher);
    $("<small></small>").html(paramCourse.teacherName)
        .appendTo(vNewColumnTeacher);
    var vNewColumnIcon = $("<div></div>").addClass("col-sm-2 text-muted text-right mt-2").appendTo(vFooter);
    $("<a></a>").attr("href", "#")
        .html('<i class="far fa-bookmark "></i>')
        .appendTo(vNewColumnIcon);
}
//Hàm hiển thị Trending course lên thẻ: 
function displayTrendingCoursesToCard(paramCourse) {
    var vTrendingList = $("#trending-cards");
    //Tạo card mới:
    var vCardCourse = $("<div></div>").addClass("card").appendTo(vTrendingList);
    //Tạo ảnh đại diện khóa học 
    $("<img></img>").addClass("card-img-top")
        .attr("src", paramCourse.coverImage)
        .appendTo(vCardCourse);
    //Tạo card body:
    var vCardBody = $("<div></div>").addClass("card-body").appendTo(vCardCourse);
    $("<h6></h6>").addClass("card-title text-primary")
        .html(paramCourse.courseName)
        .appendTo(vCardBody);
    $("<p></p>").addClass("card-text")
        .html('<i class="far fa-clock"> ' + paramCourse.duration + " " + paramCourse.level)
        .appendTo(vCardBody);
    $("<p></p>").addClass("card-text font-weight-bold")
        .html("$" + paramCourse.discountPrice + ' <del class="text-muted font-weight-normal">' + "$" + paramCourse.price + '</del> ')
        .appendTo(vCardBody);
    //Tạo footer cho card:
    var vCardFooter = $("<div></div>").addClass("card-footer").appendTo(vCardCourse);
    var vFooter = $("<div></div>").addClass("row").appendTo(vCardFooter);
    var vNewColumnTeacher = $("<div></div>").addClass("col-sm-10").appendTo(vFooter);
    $("<img></img>").attr("src", paramCourse.teacherPhoto)
        .addClass("rounded-circle size-avatar mr-2")
        .appendTo(vNewColumnTeacher);
    $("<small></small>").html(paramCourse.teacherName)
        .appendTo(vNewColumnTeacher);
    var vNewColumnIcon = $("<div></div>").addClass("col-sm-2 text-muted text-right mt-2").appendTo(vFooter);
    $("<a></a>").attr("href", "#")
        .html('<i class="far fa-bookmark "></i>')
        .appendTo(vNewColumnIcon);
}

